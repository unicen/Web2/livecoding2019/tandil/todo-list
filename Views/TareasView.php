<?php

require_once('libs/Smarty.class.php');


class TareasView {

    function __construct(){

    }

    public function DisplayTareas($tareas) {

        $smarty = new Smarty();
        $smarty->assign('titulo',"Lista de Tareas");
        $smarty->assign('BASE_URL',BASE_URL);
        $smarty->assign('lista_tareas',$tareas);
        $smarty->display('templates/ver_tareas.tpl');
    }

    public function DisplayTareasCSR(){
        $smarty = new Smarty();
        $smarty->assign('titulo',"Lista de Tareas CSR");
        $smarty->assign('BASE_URL',BASE_URL);
        $smarty->display('templates/ver_tareas_csr.tpl');
    }

    public function showError($msg) {
        echo $msg;
    }
}

?>